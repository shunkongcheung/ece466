import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

//usage
//for poisson3, java Sender localhost 0 
//for movietrace, java Sender localhost 1
//for ethernet, java Sender localhost 2

public class Sender {

	InetAddress addr = null;
	DatagramSocket socket = new DatagramSocket();

	public Sender(String hostName)throws IOException {
		addr = InetAddress.getByName(hostName);
	}

	public void SendOnePacket(int seqNo, int size)throws IOException {

		int times = 1;
		int lastpacketsize = size % 64000;
		if(size > 64000){
			times = size / 64000 + 1;
			size = 64000;
		}
		
		for(int loop = 0; loop < times; loop ++){
			
			if(loop == (times-1)){
				size = lastpacketsize;
			}
			// create traffic
			byte[]      buf = new byte[size];
			
			// copy id's into byte
			String id = Integer.toString(seqNo);
			byte[] idByte = id.getBytes();

			if(idByte.length <= buf.length){
				for(int i = 0; i < idByte.length; i ++){
					buf[i] = idByte[i];
				}
			}

			// send packet
			 DatagramPacket packet =
	                 new DatagramPacket(buf, size, addr, 4440);

			socket.send(packet);
	
		}		
	}

	public void SendAllInList(ArrayList<Frame> list)throws IOException, InterruptedException{
		
		for(Frame curFrame : list){
			SendOnePacket(curFrame.GetSeq(), curFrame.GetSize());
			TimeUnit.MICROSECONDS.sleep((long)curFrame.GetTime());
		}
	}


  public static void main(String[] args) throws Exception {
    
		// read file
		ReadFileWriteFile myreader = new ReadFileWriteFile();
		ArrayList<Frame> frameList = null;
		
		switch(Integer.parseInt(args[1]) ){
		case 0: frameList = myreader.ReadFromFile("poisson3.data", 0);break;
		case 1: frameList = myreader.ReadFromFile("movietrace.data", 1);break;
		case 2: frameList = myreader.ReadFromFile("BC-pAug89-small.TL", 2);break;
		default: break;
		}

		// send one packet
		Sender me = new Sender(args[0]);
		me.SendAllInList(frameList);

  }
}

