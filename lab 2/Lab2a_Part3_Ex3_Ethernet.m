clc;clear all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Reading the data and putting the first 100000 entries in variables 
%Note that time is in seconds and framesize is in Bytes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%data of trace file, read at traffic generator%%%%%%%%%%%%%%%%
[time1, framesize1] = textread('BC-pAug89-small.TL', '%f %f');
max = max(framesize1);

transmitTime = zeros(1,10000);
arrivalSize= zeros(1,10000);

transmitTime(1) = time1(1)*1000000;%multiply by 1000000 b/c time is in seconds
arrivalSize(1) = framesize1(1);
i = 2;
while (i <= 10000)
    transmitTime(i) = time1(i)*1000000;
    arrivalSize(i) = framesize1(i) + arrivalSize(i-1); 
    i = i + 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%File Format%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%File has format: duration, packet size, buffer size, num of tokens%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%arrivals at bucket%%%%%%%%%%%%%%%%%%%%%%%%%%%
[time_p2, packetsize_p2, backlog, numTokens ] = textread('bucketEthernet.txt', '%f %f %f %f');
bucketTransmitTime = zeros(1,10000);
bucketArrivalSize= zeros(1,10000);

bucketTransmitTime(1) = time_p2(1);
bucketArrivalSize(1) = packetsize_p2(1);
i = 2;
while (i <= 10000)
    bucketTransmitTime(i) = time_p2(i)+ bucketTransmitTime(i-1);
    bucketArrivalSize(i) = packetsize_p2(i) + bucketArrivalSize(i-1); 
    i = i + 1;
end


%%%%%%%%%%%%%%%%%%%%%%arrivals at traffic sink%%%%%%%%%%%%%%%%%%%%%%%%%%
[packet_no_p3, packetsize_p3, time_p3] = textread('outputEthernet.txt', '%f %f %f');
sinkTransmitTime = zeros(1,10000);
sinkArrivalSize= zeros(1,10000);

sinkTransmitTime(1) = time_p3(1);
sinkArrivalSize(1) = packetsize_p3(1);
i = 2;
while (i <= 10000)
    sinkTransmitTime(i) = time_p3(i)+ sinkTransmitTime(i-1);
    sinkArrivalSize(i) = packetsize_p3(i) + sinkArrivalSize(i-1); 
    i = i + 1;
end

figure(1);
compPlot = plot(bucketTransmitTime,bucketArrivalSize,'r',sinkTransmitTime,sinkArrivalSize,'b');
hold on
compPlotLegend = legend(compPlot,'Token Bucket Arrivals', 'Traffic Sink Arrivals');
title('Ethernet: Cumulative Arrivals of Token Bucket and Sink')
xlabel('Transmit Time (Microseconds)')
ylabel('Cumulative Size (Bytes)')

%%%%%%%%second plot showing content of token bucket and backlog%%%%%%%%%%%
bucketTokens = zeros(1,10000);
bucketBacklog = zeros(1,10000);
i = 2;
while (i <= 10000)
    bucketTokens(i) = numTokens(i);
    bucketBacklog(i) = backlog(i);
    i = i + 1;
end

figure(2);
compPlot2 = plot(bucketTransmitTime,bucketTokens,'r',bucketTransmitTime,bucketBacklog,'b');
hold on
compPlot2Legend = legend(compPlot2, 'Bucket Content', 'Bucket Backlog');
title('Ethernet: Token Bucket Content')
xlabel('Transmit Time (Microseconds)')
ylabel('Amount')