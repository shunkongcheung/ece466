
//usage
// for poisson3.data, java Shaper 0
// for movietrace.data, java Shaper 1
// for BC-pAug89-small.TL, java Shaper 2 

import java.io.*;
import TokenBucket.TokenBucket;
class Shaper {
	public static void main(String[] args) {
		// listen on port 4444, send to localhost:4445,
		// max. size of received packet is 1024 bytes,
		// buffer capacity is 100*1024 bytes,
		// token bucket has 10000 tokens, rate 5000 tokens/sec, and
		// records packet arrivals to bucket.txt).

		//TokenBucket lb  = new TokenBucket(4440, "localhost", 4441, 1024, 100*1024, 10000, 5000, "bucket.txt");

		//experiment 1
		//TokenBucket lb  = new TokenBucket(4440, "localhost", 4441, 1024, 100*1024, 100, 625000, "bucket.txt");

		//experiment 2
		//TokenBucket lb  = new TokenBucket(4440, "localhost", 4441, 1024, 100*1024, 500, 625000, "bucket.txt");

		//experiment 3
		//TokenBucket lb  = new TokenBucket(4440, "localhost", 4441, 1024, 100*1024, 100, 625000, "bucket.txt");

		//max experiment
		//TokenBucket lb  = new TokenBucket(4440, "localhost", 4441, 1024, 100*1024, 500, 625000, "bucket.txt");



//increased max size of received packet, buffer capacity for video and ethernet trace
		TokenBucket lb = null;
		switch (Integer.parseInt(args[0])){
		///*ex3.1
			case 0: lb = new TokenBucket(4440, "localhost", 4441, 1024, 100*1024, 1479, (3000000/8), "bucketPoisson.txt");break;
			case 1: lb = new TokenBucket(4440, "localhost", 4441, 65535, 100*65535, 657824, (1000000000/8) , "bucketVideoTrace.txt");break;
			case 2: lb = new TokenBucket(4440, "localhost", 4441, 65535, 100*65535, 1518, (1500000/8), "bucketEthernet.txt");break;
		//*/
		/*ex3.2
			case 0: lb = new TokenBucket(4440, "localhost", 4441, 1024, 100*1024, 4000, (1100000/8), "bucketPoisson.txt");break;
			case 1: lb = new TokenBucket(4440, "localhost", 4441, 65535, 100*65535, 1200000, (800000000/8) , "bucketVideoTrace.txt");break;
			case 2: lb = new TokenBucket(4440, "localhost", 4441, 65535, 100*65535, 10000, (1473100/8), "bucketEthernet.txt");break;
		*/
			default:break;
		}
		
		new Thread(lb).start();
	}
}
