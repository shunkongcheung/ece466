clc;clear all;

%%%%%%%%%%%%%data of trace file, read at traffic generator%%%%%%%%%%%%%%%%
[packet_no_p1, packetsize_p1, time_p1] = textread('generator.txt', '%f %f %f');

generatorTransmitTime = zeros(1,10000);
generatorArrivalSize= zeros(1,10000);

generatorTransmitTime(1) = time_p1(1);
generatorArrivalSize(1) = packetsize_p1(1);
i = 2;
while (i <= 10000)
    generatorTransmitTime(i) = time_p1(i) + generatorTransmitTime(i-1);
    generatorArrivalSize(i) = packetsize_p1(i) + generatorArrivalSize(i-1); 
    i = i + 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%arrivals at bucket%%%%%%%%%%%%%%%%%%%%%%%%%%%
[time_p2, packetsize_p2, backlog, numTokens ] = textread('bucket.txt', '%f %f %f %f');
bucketTransmitTime = zeros(1,10000);
bucketArrivalSize= zeros(1,10000);

bucketTransmitTime(1) = time_p2(1);
bucketArrivalSize(1) = packetsize_p2(1);
i = 2;
while (i <= 10000)
    bucketTransmitTime(i) = time_p2(i)+ bucketTransmitTime(i-1);
    bucketArrivalSize(i) = packetsize_p2(i) + bucketArrivalSize(i-1);
    i = i + 1;
end

%%%%%%%%%%%%%%%%%%%%%%arrivals at traffic sink%%%%%%%%%%%%%%%%%%%%%%%%%%
[packet_no_p3, packetsize_p3, time_p3] = textread('output.txt', '%f %f %f');
sinkTransmitTime = zeros(1,10000);
sinkArrivalSize= zeros(1,10000);

sinkTransmitTime(1) = time_p3(1);
sinkArrivalSize(1) = packetsize_p3(1);
i = 2;
while (i <= 10000)
    sinkTransmitTime(i) = time_p3(i)+ sinkTransmitTime(i-1);
    sinkArrivalSize(i) = packetsize_p3(i) + sinkArrivalSize(i-1);
    i = i + 1;
end

figure(1);
l2p2plot = plot(generatorTransmitTime,generatorArrivalSize,'r',bucketTransmitTime, bucketArrivalSize,'g',sinkTransmitTime,sinkArrivalSize,'b');
hold on
l2p2plotlegend = legend(l2p2plot, 'Traffic Generator Trace File', 'Token Bucket','Traffic Sink Output');
%for original code
%title('Cumulative Arrivals at Trace and Traffic Sink')

%for improved code
title('Cumulative Arrivals at Generator Trace, Bucket and Traffic Sink')

xlabel('Transmit Time (Microseconds)')
ylabel('Cumulative Size (Bytes)')


%%%%%%%%second plot showing content of token bucket and backlog%%%%%%%%%%%
bucketTokens = zeros(1,10000);
bucketBacklog = zeros(1,10000);
i = 2;
while (i <= 10000)
    bucketTokens(i) = numTokens(i);
    bucketBacklog(i) = backlog(i);
    i = i + 1;
end

figure(2);
compPlot2 = plot(bucketTransmitTime,bucketTokens,'r',bucketTransmitTime,bucketBacklog,'b');
hold on
compPlot2Legend = legend(compPlot2, 'Bucket Content', 'Bucket Backlog');
title('Token Bucket Content')
xlabel('Transmit Time (Microseconds)')
ylabel('Amount')