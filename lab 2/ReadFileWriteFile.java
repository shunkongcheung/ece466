import java.io.*; 
import java.util.*; 
import java.util.ArrayList;

/* 
 *  The program reads an input file "data.txt"  that has entries of the form 
 *  0	0.000000	I	536	98.190	92.170	92.170
 *  4	133.333330	P	152	98.190	92.170	92.170
 * 	1	33.333330	B	136	98.190	92.170	92.170
 *
 * The file is read line-by-line, values are parsed and assigned to variables,
 * values are  displayed, and then written to a file with name "output.txt"  
 */

class ReadFileWriteFile {  

	int sequenceNo = 0;
	public ArrayList<Frame> ReadFromFile(String filename, int readingType){
		
		BufferedReader bis = null; 
		ArrayList<Frame> frameList = new ArrayList<Frame>();

		try {  
			
			/*
			 * Open input file as a BufferedReader
			 */ 
			File fin = new File(filename); 
			FileReader fis = new FileReader(fin);  
			bis = new BufferedReader(fis);  

			/*
			 *  Read file line-by-line until the end of the file 
			 */
			String currentLine = null;  
			
			//for poisson3.data and movietrace.data
			float prevTime = 0;
			
			//for BC-pAug89-small.TL
			//float prevTime = 0f;
			while ( (currentLine = bis.readLine()) != null) { 
				
				/*
				 *  Parse line and break up into elements 
				 */
				StringTokenizer st = new StringTokenizer(currentLine); 
				
								
				String col1 = (readingType == 2)? null :st.nextToken(); 
				String col2 = st.nextToken(); 
				String col3  = (readingType == 1)? st.nextToken() : null; 
				String col4 = st.nextToken(); 
				
				
				/*
				 *  Convert each element to desired data type 
				 */
				if(readingType == 1)
					//for movietrace.data
					frameList.add(new Frame(
							Integer.parseInt(col1),	
							(int)Float.parseFloat(col2),	
							col3,
							Integer.parseInt(col4))
							);
				
				else if(readingType == 0){
					
					//for poisson3.data
					frameList.add(new Frame(
							Integer.parseInt(col1),	
								Integer.parseInt(col2) - (int)prevTime,	
								Integer.parseInt(col4))
								);
					prevTime = (float)Integer.parseInt(col2);
					
				}
				else{
					//for BC-pAug89-small.TL
					frameList.add(new Frame(
							sequenceNo++, 	
							(int)((Float.parseFloat(col2)*1000000 - prevTime)),	
							Integer.parseInt(col4))
							);
					prevTime = Float.parseFloat(col2)*1000000;
				}
			} 

		}catch (IOException e) {  
			// catch io errors from FileInputStream or readLine()  
			System.out.println("IOException: " + e.getMessage());  
		} 	

		return frameList;

	}

	public double GetAverageFrameSize(ArrayList<Frame> list, String frameType){

		float frameTotal = 0;
		int frameCount = 0;
		for (Frame frame: list){
			if(frame.GetType().equals(frameType)) {
				frameTotal += frame.GetSize();
				frameCount ++;
			}
		}
		return (frameTotal / frameCount);
	}

	public static void main (String[] args) { 
		
		ReadFileWriteFile me = new ReadFileWriteFile();
		ArrayList<Frame> frames = me.ReadFromFile("movietrace.data", 1);
		for (Frame frame: frames){
			System.out.println("SeqNo:  " + frame.GetSeq()); 
				System.out.println("Frame time:   " + frame.GetTime()); 
				System.out.println("Frame type:        " + frame.GetType()); 
				System.out.println("Frame size:       " + frame.GetSize() + "\n"); 
		} 
		System.out.println("Average size of I Frame: " + me.GetAverageFrameSize(frames, "I") +" bytes");
		System.out.println("Average size of B Frame: " + me.GetAverageFrameSize(frames, "B") +" bytes");
		System.out.println("Average size of P Frame: " + me.GetAverageFrameSize(frames, "P") +" bytes");


	}  

}

