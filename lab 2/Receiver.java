//sink

//usage
//java Receiver [output file name]
//to sync up with matlab plotting code
// for poisson3.data, java Receiver outputPoisson.txt
// for movietrace.data, java Receiver outputVideoTrace.txt
// for BC-pAug89-small.TL, java Receiver outputEthernet.txt

import java.io.*;
import java.net.*;
public class Receiver {
  public static void main(String[] args) throws IOException {
    DatagramSocket socket = new DatagramSocket(4441);

	//byte[] buf = new byte[600];
	//byte[] buf = new byte[4096];
	byte[] buf = new byte[65536];
	int counter = 1;
	PrintStream pout = null;
	
	String outputFileName = null;
	if (Integer.parseInt(args[0]) == 0){
		outputFileName = "outputPoisson.txt";
	} else if (Integer.parseInt(args[0]) == 1){
		outputFileName = "outputVideoTrace.txt";
	} else if (Integer.parseInt(args[0]) == 2){
		outputFileName = "outputEthernet.txt";
	}
	
	FileOutputStream fout =  new FileOutputStream(outputFileName);
	
	pout = new PrintStream (fout);

	long start = 0;
	int SeqNo=1;
	while(true){
		
		DatagramPacket p = new DatagramPacket(buf, buf.length);
		System.out.println("Waiting ..."); 
		socket.receive(p);
		String s = new String(p.getData(), 0, p.getLength());
		System.out.println(p.getAddress().getHostName() + ": " + s + ": " + s.length() + ": " + counter++);
		
		/*
		 *  Write line to output file 
		 */
		long receiveTime = System.nanoTime();

		if (start==0){
			start = receiveTime;
		}
			//divide diffTime by 1000 to get microsecond
				
		long diffTime = receiveTime - start;

		pout.println(SeqNo + "\t" + p.getLength() + "\t" + diffTime/1000);
		start = receiveTime;
		SeqNo++;
	}
  }
}
