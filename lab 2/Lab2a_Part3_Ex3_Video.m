clc;clear all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Reading data from the file
%Note: - time is in miliseconds and framesize is in Bytes
%      - file is sorted in transmit sequence
%  Column 1:   index of frame (in display sequence)
%  Column 2:   time of frame in ms (in display sequence)
%  Column 3:   type of frame (I, P, B)
%  Column 4:   size of frame (in Bytes)
%  Column 5-7: not used
%
% Since we are interested in the transmit sequence we ignore Columns 1 and
% 2. So, we are only interested in the following columns: 
%       Column 3:  assigned to type_f
%       Column 4:   assigned to framesize_f
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[index, time, type_f, framesize_f, dummy1, dymmy2, dymmy3 ] = textread('movietrace.data', '%f %f %c %f %f %f %f');

transmitTime = zeros(1,10000);
arrivalSize= zeros(1,10000);

transmitTime(1) = 0;%multiply by 1000 b/c time is in milliseconds
arrivalSize(1) = framesize_f(1);
i = 2;
while (i <= 10000)
    transmitTime(i) = 33333.33 + transmitTime(i-1);
    arrivalSize(i) = framesize_f(i) + arrivalSize(i-1); 
    i = i + 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%File Format%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%File has format: duration, packet size, buffer size, num of tokens%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%arrivals at bucket%%%%%%%%%%%%%%%%%%%%%%%%%%%
[time_p2, packetsize_p2, backlog, numTokens ] = textread('bucketVideoTrace.txt', '%f %f %f %f');
bucketTransmitTime = zeros(1,10000);
bucketArrivalSize= zeros(1,10000);

bucketTransmitTime(1) = time_p2(1);
bucketArrivalSize(1) = packetsize_p2(1);
i = 2;
while (i <= 10000)
    bucketTransmitTime(i) = time_p2(i)+ bucketTransmitTime(i-1);
    bucketArrivalSize(i) = packetsize_p2(i) + bucketArrivalSize(i-1); 
    i = i + 1;
end

%%%%%%%%%%%%%%%%%%%%%%arrivals at traffic sink%%%%%%%%%%%%%%%%%%%%%%%%%%
[packet_no_p3, packetsize_p3, time_p3] = textread('outputVideoTrace.txt', '%f %f %f');
sinkTransmitTime = zeros(1,10000);
sinkArrivalSize= zeros(1,10000);

sinkTransmitTime(1) = time_p3(1);
sinkArrivalSize(1) = packetsize_p3(1);
i = 2;
while (i <= 10000)
    sinkTransmitTime(i) = time_p3(i)+ sinkTransmitTime(i-1);
    sinkArrivalSize(i) = packetsize_p3(i) + sinkArrivalSize(i-1); 
    i = i + 1;
end

figure(1);
compPlot = plot(bucketTransmitTime,bucketArrivalSize,'r',sinkTransmitTime,sinkArrivalSize,'b');
hold on
compPlotLegend = legend(compPlot,'Token Bucket Arrivals', 'Traffic Sink Arrivals');
title('Video: Cumulative Arrivals of Trace, Token Bucket, Sink')
xlabel('Transmit Time (Microseconds)')
ylabel('Cumulative Size (Bytes)')

%%%%%%%%second plot showing content of token bucket and backlog%%%%%%%%%%%
bucketTokens = zeros(1,10000);
bucketBacklog = zeros(1,10000);
i = 2;
while (i <= 10000)
    bucketTokens(i) = numTokens(i);
    bucketBacklog(i) = backlog(i);
    i = i + 1;
end

figure(2);
compPlot2 = plot(bucketTransmitTime,bucketTokens,'r',bucketTransmitTime,bucketBacklog,'b');
hold on
compPlot2Legend = legend(compPlot2, 'Bucket Content', 'Bucket Backlog');
title('Video: Token Bucket Content')
xlabel('Transmit Time (Microseconds)')
ylabel('Amount')