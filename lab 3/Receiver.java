//sink

//usage
//java Receiver [output file name]
//to sync up with matlab plotting code
// for poisson3.data, java Receiver outputPoisson.txt
// for movietrace.data, java Receiver outputVideoTrace.txt
// for BC-pAug89-small.TL, java Receiver outputEthernet.txt
import java.io.*;
import java.net.*;

public class Receiver {

        
    public static void main(String[] args) throws IOException {
        if(args.length != 1){
            System.out.println("Usage: java Receiver <port>");
            return;
        }
        

        Receiver me = new Receiver();
        me.Receive(Integer.parseInt(args[0]));
        
    }
    
    private void Receive(int port)throws IOException{
        
        DatagramSocket socket = new DatagramSocket(port);

        FileOutputStream fout = new FileOutputStream("receive.txt");
        PrintStream pout =  new PrintStream(fout);

        long starttime = 0;
        int seqNo = 1;

        // infinitely receive packets
        while (true) {

            
            // datagram socket for receiving packet
            byte[] buf = new byte[4096];
            DatagramPacket p = new DatagramPacket(buf, buf.length);
            socket.receive(p);
            
            // data that is received
            String s = new String(p.getData(), 0, p.getLength());
            System.out.println(seqNo + ":" + s.length() + ": " + s );

            //  calculate the difference between two packets 
            long receiveTime = System.nanoTime();
            if (starttime == 0) {starttime = receiveTime;}
            long relativeTime = receiveTime - starttime;

            //divide diffTime by 1000 to get microsecond
            pout.println(seqNo + "\t" + p.getLength() + "\t" + relativeTime / 1000);
            seqNo++;
        }
    }
}
