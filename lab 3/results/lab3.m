clc;clear all;
% 
% %%%%% exercise 1 %%%%%%%%%%%%%%
% [time_N1, packet_size_N1, discard_N1, backlog_N1, waittime_N1] = textread('ex1.3_N1_scheduler.txt', '%f %f %f %f %f');
% [time_N5, packet_size_N5, discard_N5, backlog_N5, waittime_N5] = textread('ex1.3_N5_scheduler.txt', '%f %f %f %f %f');
% [time_N9, packet_size_N9, discard_N9, backlog_N9, waittime_N9] = textread('ex1.3_N9_scheduler.txt', '%f %f %f %f %f');
% 
% figure;
% compPlot = plot(time_N1,backlog_N1,'r', time_N5,backlog_N5,'g', time_N9,backlog_N9,'b');
% hold on
% compPlotLegend = legend(compPlot,'backlog N1', 'backlog n5', 'backlog n9');
% title('backlog of different N with FIFO');
% xlabel('Transmit Time (Microseconds)');
% ylabel('Cumulative backlog Size (Bytes)');
% 
% figure;
% compPlot = plot(time_N1,waittime_N1,'r', time_N5,waittime_N5,'g', time_N9,waittime_N9,'b');
% hold on
% compPlotLegend = legend(compPlot,'waittime N1', 'waittime n5', 'waittime n9');
% title('waittime of different N with FIFO');
% xlabel('Transmit Time (Microseconds)');
% ylabel('waittime in microsecond');
% 
% figure;
% compPlot = plot(time_N1,discard_N1,'r', time_N5,discard_N5,'g', time_N9,discard_N9,'b');
% hold on
% compPlotLegend = legend(compPlot,'discard N1', 'discard n5', 'discard n9');
% title('discard of different N with FIFO');
% xlabel('Transmit Time (Microseconds)');
% ylabel('discard in byte');
% 
% %%%%% exercise 2 %%%%%%%%%%%%%%
% [time_N1, packet_size_N1, discard_N1, backlog_video_N1, waittime_video_N1, backlog_poisson_N1, waittime_poisson_N1 ] = textread('ex2.3_N1_scheduler.txt', '%f %f %f %f %f %f %f');
% [time_N5, packet_size_N5, discard_N5, backlog_video_N5, waittime_video_N5, backlog_poisson_N5, waittime_poisson_N5] = textread('ex2.3_N5_scheduler.txt', '%f %f %f %f %f %f %f');
% [time_N9, packet_size_N9, discard_N9, backlog_video_N9, waittime_video_N9, backlog_poisson_N9, waittime_poisson_N9] = textread('ex2.3_N9_scheduler.txt', '%f %f %f %f %f %f %f');
% 
% 
% % figure;
% % compPlot = plot(time_N1,discard_N1,'r', time_N5,discard_N5,'g', time_N9,discard_N9,'b');
% % hold on
% % compPlotLegend = legend(compPlot,'discard N1', 'discard n5', 'discard n9');
% % title('discard of different N with Static Priority');
% % xlabel('Transmit Time (Microseconds)');
% % ylabel('discard in byte');
% % 
% % 
% % figure;
% % compPlot = plot(time_N1,waittime_video_N1,'r', time_N1, waittime_poisson_N1,'g');
% % hold on
% % compPlotLegend = legend(compPlot,'waittime video N1', 'waittime poisson N1');
% % title('waittime of video vs poission when N=1 with Static Priority');
% % xlabel('Transmit Time (Microseconds)');
% % ylabel('waittime in microsecond');
% 
% figure;
% compPlot = plot(time_N1,waittime_poisson_N1,'r', time_N5, waittime_poisson_N5,'g', time_N9,waittime_poisson_N9,'b');
% hold on
% compPlotLegend = legend(compPlot,'waittime N1', 'waittime n5', 'waittime n9');
% title('waittime of poisson with Static Priority');
% xlabel('Transmit Time (Microseconds)');
% ylabel('waittime in microsecond');



%%%%% exercise 2 %%%%%%%%%%%%%%
[time, packet_size_N1, discard_N1, backlog_N1, waittime_N1, backlog_N5, waittime_N5 , backlog_N9, waittime_N9] = textread('ex3.2_scheduler.txt', '%f %f %f %f %f %f %f %f %f');

figure;
compPlot = plot(time,backlog_N1,'r', time,backlog_N5,'g', time,backlog_N9,'b');
hold on
compPlotLegend = legend(compPlot,'backlog N8', 'backlog N6', 'backlog N2');
title('backlog of different N with WRR (N=8,6,2, w=3,1,1)');
xlabel('Transmit Time (Microseconds)');
ylabel('Cumulative backlog Size (Bytes)');

figure;
compPlot = plot(time,waittime_N1,'r', time,waittime_N5,'g', time,waittime_N9,'b');
hold on
compPlotLegend = legend(compPlot,'waittim N8', 'waittim N6', 'waittim N2');
title('waittime of different N with WRR(N=8,6,2, w=1,1,1)');
xlabel('Transmit Time (Microseconds)');
ylabel('waittime in microsecond');

figure;
compPlot = plot(time,discard_N1,'r');
hold on
compPlotLegend = legend(compPlot,'discard N1');
title('discard with WRR(N=8,6,2, w=3,1,1)');
xlabel('Transmit Time (Microseconds)');
ylabel('discard in byte');
