import PacketScheduler.*;

import java.io.*; 
import java.util.*; 
import java.net.*;
import java.lang.*;

// usage: java TrafficScheduler

class TrafficScheduler {  
	public static void main (String[] args) { 
     /* 
     * Create a new packet scheduler. 
     * Scheduler listens on UDP port 4444 for incoming *packets 
     * and sends outgoing packets to localhost:4445. 
     * Transmission rate of scheduler is 2Mbps. The scheduler 
     * has 2 queues, and accepts packets of maximum size 1024 
     * bytes. 
     * Capacity of first queue is 100*1024 bytes and capacity 
     * if second queue is 200*1024 bytes. 
     * Arrivals of packets are recorded to file ps.txt. 
     */ 

	if (args.length != 5) {
    	System.out.println("Usage: java TrafficScheduler  <in_port> <out_port> <buffer_size> <buffer_count> <capacity_byte> ");
    	return;
	}

	// exercise 1.2 and 1.3: buffer size 100kB, 1 buffer, capacity 10Mbps
	// java TrafficScheduler 4440 4441 100000 1 1250000
    
	// exercise 2.2: buffer size 100kB, 2 buffers, capacity 20Mbps
	// java TrafficScheduler 4440 4441 100000 2 2500000

	// exercise 3.1: buffer size 100kB, 3 buffer, capacity 10Mbps
	// java TrafficScheduler 4440 4441 100000 3 1250000

	// retreiveing information from inputs
	int in_port = Integer.parseInt(args[0]);
	int out_port = Integer.parseInt(args[1]);

	int buffer_size = Integer.parseInt(args[2]);
	int buffer_count = Integer.parseInt(args[3]);

	int capacity= Integer.parseInt(args[4]);

	// dynamic allocate memory
	long [] bufferCapacity = new long[buffer_count];
	for(int i = 0; i < buffer_count; i ++)
		bufferCapacity[i] = capacity;


    // Scheduler
     PacketScheduler ps2 = new PacketScheduler(in_port, "localhost", out_port, 
        buffer_size, buffer_count, 2024, bufferCapacity, "scheduler.txt"); 
        
     // start packet scheduler 
     new Thread(ps2).start(); 
		
		 
	}  
}



