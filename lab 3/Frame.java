
class Frame{
	private int SeqNo;
	private int Ftime;
	private String Ftype;
	private int Fsize;	

	public Frame(int seqNo, int time, String type, int size){
		SeqNo = seqNo;
		Ftime = time;
		Ftype = type;
		Fsize = size;
	}

	public Frame(int seqNo, int time, int size){
		SeqNo = seqNo;
		Ftime = time;
		Ftype = null;
		Fsize = size;
	}

	public int GetSeq(){return SeqNo;}
	public int GetTime(){return Ftime;}
	public String GetType(){return Ftype;}
	public int GetSize(){return Fsize;}
}
