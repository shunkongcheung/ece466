
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

//usage
//for poisson3, java Sender localhost 0 
//for movietrace, java Sender localhost 1
//for ethernet, java Sender localhost 2
public class Sender {

    // sockets
    InetAddress addr = null;
    DatagramSocket socket = new DatagramSocket();

    //  this sender's priority (an integer that is stored in string)
    String priority = "0";
    
    // output log
    FileOutputStream fout;
    PrintStream pout;
    long starttime = 0;

    int port = 4440;
    
    public Sender(String hostName, String outputfilename, String _priority, int _port) throws IOException {
        addr = InetAddress.getByName(hostName);
        
        priority = _priority;
        
         fout = new FileOutputStream(outputfilename);
          pout = new PrintStream(fout);
	
	port = _port;
          
    }

    public void SendOnePacket(int seqNo, int size) throws IOException {

        // packet cannot exceed datagram size
        int times = 1;
        int lastpacketsize = size % 64000;
        if (size > 64000) {
            times = size / 64000 + 1;
            size = 64000;
        }

        // for each sub datagram
        for (int loop = 0; loop < times; loop++) {

            if (loop == (times - 1)) {
                size = lastpacketsize;
            }
            // create traffic
	    if(size < 1)size = 1;

            byte[] buf = new byte[size];
            
            // write priority into the first few bytes
            byte[] idByte = priority.getBytes();
            if (idByte.length <= buf.length) {
                for (int i = 0; i < idByte.length; i++) {
                    buf[i] = idByte[i];
                }
            }
	    else{
		System.err.println("Buffer size is too small: " + size);
	    }

            
            // send packet
            DatagramPacket packet = new DatagramPacket(buf, size, addr, port);
            socket.send(packet);
        }
        
        // get current time and calculate difference 
        long currtime = System.nanoTime();
        if (starttime == 0) {starttime = currtime;}
        long relativeTime = currtime - starttime;

        // print difference to log
        pout.println(seqNo + "\t" + size + "\t" + (relativeTime / 1000));
        
    }

    public void SendAllInList(ArrayList<Frame> list, int N) throws IOException, InterruptedException {

        for (Frame curFrame : list) {
            SendOnePacket(curFrame.GetSeq(), curFrame.GetSize());
            TimeUnit.MICROSECONDS.sleep((long) (curFrame.GetTime()/ N));
        }
    }

    public void SendWithAverageRate(float T, int N, int L,
            int points,
            String outputfilename)
            throws IOException, InterruptedException {

        FileOutputStream fout = new FileOutputStream(outputfilename);
        PrintStream pout = new PrintStream(fout);

        int seqNo = 1;
        for(int curpoint = 0 ; curpoint < points; curpoint ++){
            for (int i = 0; i < N; i++) {

                // send one packet
                SendOnePacket(seqNo, L);
                seqNo++;
            }

            // wait T microseconds before sending the next batch of N packets
            TimeUnit.MICROSECONDS.sleep((long) T);
			
			// printing
			if(curpoint % 100 == 0)
				System.out.println("sending " + curpoint  + "... ");
        }
    }

    public static void main(String[] args) throws Exception {
        
    
//        if (args.length != 4) {
//            System.out.println("Usage: java Receiver <T> <N> <L> <P>");
//            return;
//        }
//        //Every T msec, transimit a group of N packets back-to-back, each with a size of L bytes
//        float T = Float.parseFloat(args[0]);
//        int N = Integer.parseInt(args[1]);
//        int L = Integer.parseInt(args[2]);
//        int P  =Integer.parseInt(args[3]);
//        
//        me.SendWithAverageRate(T, N, L, P, outputfilename);

	// ex 1.2 and 1.3 java Sender 1 0 poisson3.data
	// ex 2.2 "java Sender 1 0 movietrace.data" && "java Sender N 1 poisson3.data"

        if (args.length != 3) {
            System.out.println("Usage: java Receiver <N> <Priority> <datafile>");
            return;
        }
        
        int readFileSetting;
        if(args[2].equals("poisson3small.data") ||
           args[2].equals("poisson3.data")){
            readFileSetting = 0;
        }
	else if(args[2].equals("movietrace.data") ){
	    readFileSetting = 1;
	}
        else{
            System.out.println("Unrecoginized data file!");
            return;
        }
        
        
        // send one packet
        Sender me = new Sender("localhost", "generator.txt", args[1], 4440);
        
        ReadFileWriteFile fileRW = new ReadFileWriteFile();
        ArrayList<Frame> frames = fileRW.ReadFromFile(args[2], readFileSetting);
        
        me.SendAllInList(frames, Integer.parseInt(args[0]));
        
    }
}
