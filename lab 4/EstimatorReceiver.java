//java EstimatorReceiver

import java.io.*;
import java.net.*;
public class EstimatorReceiver {
	
	/**
	* Converts a byte array to an integer.
	* @param value
	a byte array
	* @param start
	start position in the byte array
	* @param length
	number of bytes to consider
	* @return
	the integer value
	*/
	public static int fromByteArray(byte [] value, int start, int length)
	{
		int Return = 0;
		for (int i=start; i< start+length; i++)	{
			Return = (Return << 8) + (value[i] & 0xff);
		}
		return Return;
	}
	
	public static byte[] toByteArray(int value)
	{
		byte[] Result = new byte[4];
		Result[3] = (byte) ((value >>> (8*0)) & 0xFF);
		Result[2] = (byte) ((value >>> (8*1)) & 0xFF);
		Result[1] = (byte) ((value >>> (8*2)) & 0xFF);
		Result[0] = (byte) ((value >>> (8*3)) & 0xFF);
		return Result;
	}
	
	public static void main(String[] args) throws IOException {
		
	    DatagramSocket socket = new DatagramSocket(4446);
	
		byte[] buf = new byte[65536];
//		int counter = 1;
		
		PrintStream pout = null;
		
		String outputFileName = "receiverOutput.txt";
/*		
		if (Integer.parseInt(args[0]) == 0){
			outputFileName = "outputPoisson.txt";
		} else if (Integer.parseInt(args[0]) == 1){
			outputFileName = "outputVideoTrace.txt";
		} else if (Integer.parseInt(args[0]) == 2){
			outputFileName = "outputEthernet.txt";
		}
*/		

		FileOutputStream fout =  new FileOutputStream(outputFileName);
		
		pout = new PrintStream (fout);
	
	//	long start = 0;
		int SeqNo=1;
		int portNumber = 0;
		long startTime = System.nanoTime();
		while(true){
			
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			System.out.println("Waiting ..."); 
			socket.receive(packet);
			
			portNumber = fromByteArray(packet.getData(), 0, 2);
			SeqNo = fromByteArray(packet.getData(), 2, 4);
			
			String s = new String(packet.getData(), 0, packet.getLength());
			System.out.println(packet.getAddress().getHostName() + ": Length: " + s.length() + ": " + SeqNo);
			
			/*
			 *  Write line to output file 
			 */
			long receiveTime = System.nanoTime();
			long relativeTime = receiveTime - startTime;
/*	
			if (start==0){
				start = receiveTime;
			}
				//divide diffTime by 1000 to get microsecond
					
			long diffTime = receiveTime - start;
	*/
			pout.println(SeqNo + "\t" + portNumber + "\t" + packet.getLength() + "\t" + receiveTime/1000+ "\t" + relativeTime/1000);
//			start = receiveTime;
//			SeqNo++;
		}
	}
}