//usage: java Estimator localhost 4446

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Estimator{
	
	InetAddress addr = null;
	DatagramSocket socket = new DatagramSocket();
	
	public Estimator(String hostName)throws IOException {
		addr = InetAddress.getByName(hostName);
	}
	
	/**
	* Converts an integer to a byte array.
	* @param
	value
	an integer
	* @return
	a byte array representing the integer
	*/
	public static byte[] toByteArray(int value)
	{
		byte[] Result = new byte[4];
		Result[3] = (byte) ((value >>> (8*0)) & 0xFF);
		Result[2] = (byte) ((value >>> (8*1)) & 0xFF);
		Result[1] = (byte) ((value >>> (8*2)) & 0xFF);
		Result[0] = (byte) ((value >>> (8*3)) & 0xFF);
		return Result;
	}
	
	public void SendOnePacket(InetAddress address, int seqNo, int size, int port)throws IOException {

		int times = 1;
		int lastpacketsize = size % 64000;
		if(size > 64000){
			times = size / 64000 + 1;
			size = 64000;
		}
		
		for(int loop = 0; loop < times; loop ++){
			
			if(loop == (times-1)){
				size = lastpacketsize;
			}
			// create traffic
			byte[]      buf = new byte[size];
			
			// put returnPort in first 2 bytes of buf
			System.arraycopy(toByteArray(port),2,buf,0,2);
			System.arraycopy(toByteArray(seqNo),0,buf,2,4);

			// send packet
			 DatagramPacket packet =
	                 new DatagramPacket(buf, size, address, 4440);

			socket.send(packet);
		}		
	}

	public static void main(String[] args)throws Exception{
		InetAddress ipAddr = null;

		int returnPort = 4444;

		PrintStream pout = null;
		String outputFileName = "senderSendTimes.txt";
		FileOutputStream fout =  new FileOutputStream(outputFileName);
		pout = new PrintStream (fout);
		
		if (args.length == 2){
//			ipAddr = Integer.parseInt(args[0]);
			ipAddr = InetAddress.getByName(args[0]);
			returnPort = Integer.parseInt(args[1]);
		}
		
		Estimator me = new Estimator(args[0]);
		int seqNum = 1;
		long sendTime = 0;
		long startTime = System.nanoTime();
		long relativeTime = 0;
		while(true){
			sendTime = System.nanoTime();
			relativeTime = sendTime-startTime;
			me.SendOnePacket(ipAddr, seqNum, 10, returnPort);
			pout.println(seqNum + "\t" + sendTime/1000 + "\t" + relativeTime/1000);
			seqNum++;
			
			
		}
		
	}



}
